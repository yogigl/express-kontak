const Kontak = require('../models/Kontak');

const KontakController = () => {
  // create kontak
  // body: nama, noHandphone, email
  const create = async (req, res) => {
    const { nama, noHandphone, email } = req.body;

    try {
      // create kontak baru
      const kontak = await Kontak.create({
        nama,
        noHandphone,
        email,
      });
      // jika gagal create
      if (!kontak) {
        return res.status(400).json({ msg: 'Request tidak sesuai: Model tidak ditemukan' });
      }
      // jika sukses
      return res.status(201).json({ kontak });
    } catch (error) {
      return res.status(500).json({ msg: 'Internal server error' });
    }
  };

  // daftar kontak
  const index = async (req, res) => {
    try {
      // create kontak baru
      const kontaks = await Kontak.findAll();
      // jika sukses
      return res.status(200).json({ kontaks });
    } catch (error) {
      return res.status(500).json({ msg: 'Internal server error' });
    }
  };

  // Update kontak berdasarkan id
  const update = async (req, res) => {
    const { id } = req.params;
    const { nama, email, noHandphone } = req.body;
    try {
      // find kontak berdasarkan id
      const kontak = await Kontak.find({
        where: {
          id,
        },
      });

      // jika tidak ditemukan
      if (!kontak) {
        return res.status(404).json({ msg: 'Not Found: kontak tidak ditemukan' });
      }

      // update kontak
      await Kontak.update(
        { nama, noHandphone, email },
        {
          returning: true,
          where: {
            id,
          },
        },
      );

      // find kontak berdasarkan id
      const updated = await Kontak.find({
        where: {
          id,
        },
      });

      // jika sukses
      return res.status(200).json({ kontak: updated });
    } catch (error) {
      return res.status(500).json({ msg: 'Internal server error' });
    }
  };

  // hapus kontak berdasarkan id
  const destroy = async (req, res) => {
    const { id } = req.params;
    try {
      // find kontak berdasarkan id
      const kontak = await Kontak.find({
        where: {
          id,
        },
      });

      // jika tidak ditemukan
      if (!kontak) {
        return res.status(404).json({ msg: 'Not Found: kontak tidak ditemukan' });
      }

      // destroy kontak
      await Kontak.destroy({
        where: {
          id,
        },
      });

      // jika sukses
      return res.status(204).json({ kontak });
    } catch (error) {
      return res.status(500).json({ msg: 'Internal server error' });
    }
  };
  return {
    create,
    index,
    update,
    destroy,
  };
};

module.exports = KontakController;
