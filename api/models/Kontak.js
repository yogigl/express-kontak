const Sequelize = require('sequelize');
const sequelize = require('../../config/database');

const tableName = 'kontaks';

const Kontak = sequelize.define(
  'Kontak',
  {
    nama: {
      type: Sequelize.STRING,
      allowNull: false,
      unique: true,
      min: 3,
    },
    noHandphone: {
      type: Sequelize.STRING,
      allowNull: false,
      unique: true,
      min: 8,
    },
    email: {
      type: Sequelize.STRING,
      allowNull: false,
      unique: true,
      validate: {
        isEmail: true,
        min: 5,
      },
    },
  },
  { tableName, timestamps: false },
);

module.exports = Kontak;
