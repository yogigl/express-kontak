# express-kontak

> bootsraped by [express-rest-api-boilerplate](https://github.com/aichbauer/express-rest-api-boilerplate)

## run production

- copy .env.example to .env
- `npm i`
- `npm run production`

## routes

### POST /buat

```
{
	"nama": "ini",
	"noHandphone": "08123124412",
	"email": "ini@ini.com"
}
```

### GET /daftar

### PATCH /ubah/:id

```
{
	"nama": "ini",
	"noHandphone": "08123124412",
	"email": "ini@ini.com"
}
```

### DELETE /hapus/:id
