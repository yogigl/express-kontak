const privateRoutes = require('./routes/privateRoutes');
const publicRoutes = require('./routes/publicRoutes');
const kontakRoutes = require('./routes/kontakRoutes');

const config = {
  migrate: false,
  privateRoutes,
  publicRoutes,
  kontakRoutes,
  port: process.env.PORT || '3000',
};

module.exports = config;
