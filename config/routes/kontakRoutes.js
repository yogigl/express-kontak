const kontakRoutes = {
  'POST /buat': 'KontakController.create',
  'GET /daftar': 'KontakController.index',
  'DELETE /hapus/:id': 'KontakController.destroy',
  'PATCH /ubah/:id': 'KontakController.update',
};

module.exports = kontakRoutes;
